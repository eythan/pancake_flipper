package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const (
	MIN uint = 1
	MAX uint = 100
)

type Pancake struct {
	isRightSideUp bool
}

type Stack []Pancake

type Stacks []Stack

func main() {
	var caseCount int
	var stacks []Stack

	askForCaseCount(&caseCount)

	// Get the original order every
	// stack of pancakes.
	for i := 0; i < caseCount; i++ {
		stack := askForPancakeOrder()
		stacks = append(stacks, stack)
	}

	// Sort each stack of pancakes.
	for index1, varStack := range stacks {
		var numberOfFlips uint
		for index2, varPancake := range varStack {
			if varPancake.isRightSideUp == false {
				varStack = flipStack(varStack, index2)
				numberOfFlips++
			}
		}
		fmt.Printf("Case #%d: %d\n", index1+1, numberOfFlips)
	}

}

// Get the number of cases the user provided
// (with some validation)
func askForCaseCount(caseCount *int) {
	fmt.Printf("Enter case count (between %v and %v): ", MIN, MAX)
	fmt.Scanf("%d", caseCount)

	if 1 > *caseCount || *caseCount > 100 {
		askForCaseCount(caseCount)
	}
}

// Get user input for pancake order
// and build stack of pancakes
func askForPancakeOrder() Stack {
	var stack = Stack{}
	var stackStr string

	fmt.Println("Enter order of pancake stack ('+' for smiley face up, '-' for smiley face down): ")
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		stackStr = scanner.Text()

		chars := strings.Split(stackStr, "")

		for _, char := range chars {
			switch char {
			case "+":
				stack = append(stack, Pancake{
					true,
				})
			case "-":
				stack = append(stack, Pancake{
					false,
				})
			}
		}
		break
	}
	return stack
}

// Flip the stack at the given position.
func flipStack(s Stack, position int) Stack {
	stackPartial := s[position:]
	for index, pancake := range stackPartial {
		stackPartial[index].isRightSideUp = (pancake.isRightSideUp == false)
	}

	return s

}
